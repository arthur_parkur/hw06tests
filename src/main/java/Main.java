public class Main {

    public static void main(String[] args) {
        //method1(new int[]{2, 5, 4, 5, 4, 3, 0});
        //method2(new int[]{2, 5, 4, 5, 4, 3, 0});
    }

    public static int[] method1(int[] arr) {
        int l = arr.length;
        int last4Index = 0;

        for (int i = l - 1; i >= 0; i--) {
            if (arr[i] == 4) {
                last4Index = i + 1;
                break;
            }
        }

        if (last4Index == 0) throw new RuntimeException();
        int[] nArr = new int[l - last4Index];
        for (int i = last4Index, n = 0; i < l; i++, n++) {
            nArr[n] = arr[i];
        }
        return nArr;
    }

    public static boolean method2(int[] arr) {
        boolean haveOne = false;
        boolean haveFour = false;
        int len = arr.length;
        for (int i = 0; i < len; i++) {
            if (arr[i] != 1 && arr[i] != 4) return false;
            if (arr[i] == 1) haveOne = true;
            if (arr[i] == 4) haveFour = true;
        }
        return haveOne && haveFour;
    }
}
