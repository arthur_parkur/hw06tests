import org.junit.*;

import java.sql.*;

public class MainTest {
    private static Connection connection;
    private static Statement statement;
    private static final String createRow = "INSERT INTO Students \n " +
            "(name, mark) VALUES (?, ?)";
    private static final String createTable = "CREATE TABLE IF NOT EXISTS Students \n" +
            "(id INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
            "name TEXT, \n" +
            "mark INTEGER)";
    private static final String clearTable = "DELETE FROM Students";

    @BeforeClass
    public static void startTest() {
        System.out.println("MainTest started");
        connect();
        createAndFillTable();
    }

    @Test
    public void testMethod1Correct() {
        int[] a = {3, 0};
        Assert.assertArrayEquals(a, Main.method1(new int[]{2, 5, 4, 5, 4, 3, 0}));
    }

    @Test(expected = RuntimeException.class)
    public void testMethod1Empty() {
        int[] a = {3, 0};
        Assert.assertEquals(a, Main.method1(new int[]{2, 5}));
    }

    @Test
    public void testMethod2Correct() {
        Assert.assertEquals(true, Main.method2(new int[]{1, 4}));
    }

    @Test
    public void testMethod2inCorrect() {
        Assert.assertEquals(false, Main.method2(new int[]{1, 1}));
    }

    //INSERT
    @Test(expected = SQLException.class)
    public static void insertCorrect() throws SQLException {
        String sql = "INSERT INTO Students (name, mark) VALUES ('TestStud', 222);";
        Assert.assertEquals(1, statement.executeUpdate(sql));
    }

    //UPDATE
    @Test(expected = SQLException.class)
    public static void updateCorrect() throws SQLException {
        String sql = "UPDATE Students SET mark = 333 WHERE mark = 222";
        Assert.assertEquals(1, statement.executeUpdate(sql));
    }

    //DELETE
    @Test(expected = SQLException.class)
    public static void deleteCorrect() throws SQLException {
        String sql = "DELETE FROM Students WHERE name = 'TestStud'";
        Assert.assertEquals(1, statement.executeUpdate(sql));
    }

    @AfterClass
    public static void endTests() {
        disconect();
        System.out.println("MainTest ended");
    }

    public static void connect() {
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:hw02db.db");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void disconect() {
        try {
            connection.close();
        } catch (SQLException e) {

        }
    }

    public static void createAndFillTable() {
        try {
            statement = connection.createStatement();

            statement.execute(createTable);
            //statement.execute(clearTable);

            PreparedStatement ps = connection.prepareStatement(createRow);

            connection.setAutoCommit(false);
            for (int i = 0; i < 10; i++) {
                ps.setString(1, "Студент" + i);
                ps.setInt(2, i * 10);
                ps.addBatch();
            }
            ps.executeBatch();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
